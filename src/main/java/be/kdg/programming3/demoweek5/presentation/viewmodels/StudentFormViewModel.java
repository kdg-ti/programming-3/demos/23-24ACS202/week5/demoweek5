package be.kdg.programming3.demoweek5.presentation.viewmodels;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Past;
import jakarta.validation.constraints.Positive;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;

public class StudentFormViewModel {
    @NotBlank(message = "firstname should not be empty!")
    private String firstname;

    @NotBlank(message = "lastname should not be empty!")
    private String lastname;

    @Past(message = "your birthday should be in the past...")
    private LocalDate birthdate;

    @Positive(message = "Length should be positive")
    private Double length;
    private Integer credits;

    public StudentFormViewModel() {
    }

    public StudentFormViewModel(String firstname, String lastname, LocalDate birthdate, Double length, Integer credits) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthdate = birthdate;
        this.length = length;
        this.credits = credits;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public void setCredits(Integer credits) {
        this.credits = credits;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public Double getLength() {
        return length;
    }

    public Integer getCredits() {
        return credits;
    }

    @Override
    public String toString() {
        return "StudentFormViewModel{" +
                "firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", birthdate=" + birthdate +
                ", length=" + length +
                ", credits=" + credits +
                '}';
    }
}
