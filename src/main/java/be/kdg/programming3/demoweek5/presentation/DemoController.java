package be.kdg.programming3.demoweek5.presentation;

import be.kdg.programming3.demoweek5.presentation.viewmodels.PersonViewModel;
import be.kdg.programming3.demoweek5.presentation.viewmodels.StudentFormViewModel;
import be.kdg.programming3.demoweek5.service.StudentService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Controller
public class DemoController {
    private static Logger logger = LoggerFactory.getLogger(DemoController.class);
    private StudentService studentService;

    public DemoController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("/hello")
    public String sayHello(@RequestParam("firstname") Optional<String> fname,
                           @RequestParam("lastname") Optional<String> lastname,
                           Model model) {
        logger.info("Got request parameters:" + fname + "," + lastname);
        model.addAttribute("firstname", fname.orElse("dummy"));
        model.addAttribute("lastname", lastname.orElse("dummy"));
        return "hello";
    }

    //Generate a random number based on the request parameters...
    @GetMapping("/randomnumber")
    public String generateRandomNumberPage(@RequestParam("maxvalue") List<Integer> values,
                                           @RequestParam(value = "even", required = false) boolean even,
                                           Model model) {
        if (values == null || values.isEmpty()) {
            logger.error("Parameter maxvalue not optional!");
            throw new RuntimeException("It is not optional!");//we will learn how to handle errors in Spring later...
        }
        Random random = new Random();
        int randomNumber = 0;
        if (values.size() == 1) {
            randomNumber = even ? 2 * random.nextInt(values.get(0) / 2) : random.nextInt(values.get(0));
        } else if (values.size() > 1) {
            randomNumber = even ? 2 * random.nextInt(values.get(0) / 2, values.get(1) / 2)
                    : random.nextInt(values.get(0), values.get(1));
        }
        model.addAttribute("randomnumber", randomNumber);
        return "random";
    }
    @GetMapping({"/repeater/{word}", "/repeater"})
    public String repeatWord(@PathVariable("word") Optional<String> word,
                             @RequestParam("repeat") Optional<Integer> repeat,
                             Model model) {
        model.addAttribute("repeatedword", word.orElse("dummy").repeat(repeat.orElse(10)));
        return "repeater";
    }

/*    @PostMapping("/students/add")
    public String processAddStudent(@RequestParam("fname") String firstname,
                                    String lastname,
                                    LocalDate birthdate,
                                    Double length,
                                    Integer credits) {
        logger.info("Processing " + firstname + " " + lastname + " " + birthdate + " " + length + " " + credits);
        //…
        return "addstudent";
    }*/

    @PostMapping("/students/add")
    public String processAddStudent(@Valid @ModelAttribute("studentformviewmodel") StudentFormViewModel viewModel,
                                    BindingResult result) {
        logger.info("Processing " + viewModel.toString());
        if (result.hasErrors()) {
            result.getAllErrors().forEach(e->logger.warn(e.toString()));
        } else {
            logger.info("No validation errors, adding the student...");
            studentService.addStudent(viewModel.getFirstname(),
                    viewModel.getLastname(),
                    viewModel.getBirthdate(),
                    viewModel.getLength(),
                    viewModel.getCredits());
        }
        return "addstudent";
    }

    @GetMapping("/students/add")
    public String getAddStudentForm(Model model){
        //add an empty viewmodel to the form, to be able to use it in thyemleaf to
        //generate the name= and id= attributes....
        model.addAttribute("studentformviewmodel", new StudentFormViewModel());
        return "addstudent";
    }

    @GetMapping("/yourage")
    public String getYourAge(Model model){
        model.addAttribute("person", new PersonViewModel());
        return "yourage";
    }

/*    @PostMapping("/yourage")
    public String postYourAge(PersonViewModel viewModel, Model model){
        //calculate the age...
        int age = LocalDate.now().getYear() - viewModel.getBirthday().getYear();
        model.addAttribute("age", age);
        model.addAttribute("person", viewModel);
        return "yourage";
    }*/

    @PostMapping("/yourage")
    public String postYourAge(PersonViewModel viewModel, Model model){
        model.addAttribute("person", viewModel);
        return "yourage";
    }
}
