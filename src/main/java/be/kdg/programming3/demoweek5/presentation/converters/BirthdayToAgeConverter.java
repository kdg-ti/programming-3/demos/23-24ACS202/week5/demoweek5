package be.kdg.programming3.demoweek5.presentation.converters;

import org.springframework.core.convert.converter.Converter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class BirthdayToAgeConverter implements Converter<String, Integer> {
    @Override
    public Integer convert(String source) {
        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate birthday = LocalDate.parse(source);//, formatter);
        return LocalDate.now().getYear() - birthday.getYear();
    }
}
