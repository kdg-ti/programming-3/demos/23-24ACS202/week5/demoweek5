package be.kdg.programming3.demoweek5.presentation.viewmodels;

import java.time.LocalDate;

public class PersonViewModel {
    private String name;
    private int age;

    public PersonViewModel(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public PersonViewModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "PersonViewModel{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
