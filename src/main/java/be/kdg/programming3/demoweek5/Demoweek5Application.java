package be.kdg.programming3.demoweek5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demoweek5Application {

    public static void main(String[] args) {
        SpringApplication.run(Demoweek5Application.class, args);
    }

}
